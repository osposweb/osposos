# VenenuX Open Source Point Of Sale

Contruccion de OS Live: imagen live debian/devuan VENENUX para el sistema web osposweb. 
For english speakers [README-en.md](README-en.md) but take in consideration we dont care speak in english.

Es un sistema operativo listo para usar que incluye el sistema de POSWEB asi como un 
programa para gestion remota como el componente mas critico para asistir los usuarios.

## Introduccion

Este trabajo es **consecuencia de la complejidad que es no solo el instalar, sino el poner a punto 
el sistema OSPOS**, ademas de lo **complicado que es tener un Docker en linea que implica tambien 
tener un sistema operativo e instalar** con el mismo nivel de conocimiento requerido.

Con esta ISO, se graba tanto en USB como en CD/DVD y simplemente se arrsnca en un computador, 
que es el proceso que los mortales ignorantes conocemos? obviamente si.

Se recomeinda visite el proyecto  @venenux para informacion acerca de VenenuX, los paquetes base, 
el porque se incluyen y porque no. VenenuX es un sistema operativo derivado de Debian GNU/Linux 
enfocado en multimedia, no solo profesional, sino tambine para el usuario que lo desea ver todo.

## FAQ: preguntas y repuestas frecuentes

#### P: Puedo usarlo como sistema habitual?

Si, tiene una gran cantidad de programas, inclusive tiene un completo entorno de desarrollo, 
para mas info rapida de que tiene vistar [venenux-contenido-ospos-livebuild.md](venenux-contenido-ospos-livebuild.md)

#### P: Impresoras Fiscales: como utilizo mi equipo o impresora fiscal

Hay dos maneras, una simple y una que esta en camino. La mas simple (no tanto) es realizar 
una adecuacion (AD-HOC) especifica para su caso, enviar el comando fiscal en lote por un 
archivo y invocar este comando justo desde el controller de Sales al terminar una venta.
Para eso contacte la pagina web http://www.osposweb.com y le realizaremos una cotizacion.

La mejor manera (pero en desarrollo) es emplear un FiscalBerry/FiscalBox https://paxapos.github.io/fiscalberry/
este equipo centralia el dispositivo y premite utilizarlo remotamente, con este ud empleara 
su impresora lejos de su computador cliente, ya que el servidor le enviara los comandos 
fiscales ademas de aventajar el no tener muchos cables teneindo uan buena presentacion al cleinte.

#### P: impresoras normales o no fiscales son compatibles

Para casos donde el entonro fiscal no es mandatorio, simplemente se monta 
un live/usb y se ejecuta, el cliente feliz ya que todo anda rapido y no ve instalacion alguna. 
En estos casos se debe configurar especificamente la impresora, tenga en cuenta hay dos casos, 
las impresoras normales (forma libre) y las matrix no fiscales (recipes), favor 
referirse a la documentacion: [venenux-configurar-impresoras-nofiscales-ospos.md](venenux-configurar-impresoras-nofiscales-ospos.md)

## Informacion rapida de lb_build

Este sistema se creo utilizando los paquetes VenenuX y el sistema Live-build debian, 

## Comenzar rapido: Guia de ejcucion rapida

Si esta apurado, comience aqui: [venenux-live-howto-livebuild.md](venenux-live-howto-livebuild.md) (solo español)

### Guid resumida live-build VenenuX

live build crea una estructura con la cual se puede alterar el resultado de el live cd, 
aqui mostrada, mas abajo la documentacion especifica de esto y como hacer tu propia iso:

```
├── lb-scriptpropio        <--------- script creado con la invocacion a lb config
└── config
    ├── archives           <--------- xxxx.list.yyyy sources.list de repositorios ajenos
    ├── bootloaders
    │   └── isolinux       <--------- contenido COMPLETO base de el cd/img resultante, incluyendi menu.cfg
    ├── hooks
    ├── includes.binary    <--------- archivos extra que terminaran incluidos en el cd o imagen, en raiz
    ├── includes.chroot
    │   ├── etc
    │   │   ├── lightdm    <--------- queremos meter lxdm pero systemd no nos deja, versiones 0.x usan lxdm/slim
    │   │   ├── live       <--------- live user setups scripts
    │   │   └── skel       <--------- skeleton /home directory, plantilla de como home se preconfigura
    │   └── usr
    │       ├── bin       <--------- colocar aqui cualquier programa precompilado o directo
    │       ├── lib       <--------- colocar aqui cualquier precompilada precompilado o directo
    │       ├── local
    │       └── share       <--------- colocar aqui cualquier oro archiovo e ira a share en el live
    ├── includes.installer <--------- installer preseed.cfg file and logo/theme update for the graphical installer
    │   └── usr
    │       └── share       <--------- colocar aqui cualquier oro archiovo e ira a share en el live
    │           └── graphics  <--------- logo/theme update for the graphical installer
    ├── package-lists      <--------- listado de nombre of packages to be installed, uno por linea
    └── packages.chroot    <--------- otros packages to be installed manualmente o por archivo deb
```

### Documentacion rapida live build:

LIVE IMAGEN USABLE: es una imagen ISO/USB mix, como si de un disco duro se tratase, 
se revise el directorio `debianlive` por ejemplo para jessie [debianlive8](debianlive8) 
esta instalacion trae `mate`,  `apache2`, `php`, `mariadb`, `firefox`, `git`, y `osposweb` y 
este hay que instalarlo antes de usarlo, tambien trae software de redes y multimedia.

USB IMAGEN DIRECTA: es una instalacion directa, como si de un disco duro se tratase, 
se revise el directorio `debianusb` por ejemplo para jessie [debianusb8](debianusb8) 
esta instalacion trae `apache2`, `php`, `mariadb`, `firefox`, `git`, y `osposweb` y 
este si trae en ejecucion el software listo para usar.
