# Instalacion local desde iso
----------------------

Descargar la ISO desde https://sourceforge.net/projects/vegnuli/files/VenenuX-1.0/venenux-1.0-osposweb/debian-venenux-8-osposweb-i386.hybrid.iso/download

1. **Descargar** desde el sitio https://sourceforge.net/projects/vegnuli/files/VenenuX-1.0/venenux-1.0-osposweb/debian-venenux-8-osposweb-i386.hybrid.iso/download 
esto guardara un archivo `.ISO` en el computador, asumamos sera `Descargas`.
2. **Arrancar** esta iso, la PC debe inicicar el dispositivo donde se "volco" esta iso, 
si es USB es asi en linux: `cp debian-venenux-8-osposweb-i386.hybrid.iso /dev/<usbdisk>` 
esto borrara todo dato en el usbdrive, si es en otro sistema operativo debe quemarlo en un DVD o BD.
3. **Esperar** hasta que arranque el escritorio, que muestre una pantalla y se inicie el navegador web `firefox`. 
el navegador mostrara uan pagina indicando que el ospos esta listo para arrancar
4. **Abrir** el terminal de comandos, desde el menu izquierdo inferior, `Menu`->`Herramientas Sistema`->`Sakura` 
y la ventana de fondo negro aparecera para escribir comandos.
5. **Escalar** privilegios de administrador `root` ejecutando en el `Terminal` el comando: `sudo su`, 
es recomendable maximizar la ventana para ver en contexto los mensajes de error si alguno
6. **Usuario** de acceso a la base de datos, ejecutando el comando 
`mysql -u root -e "CREATE SCHEMA ospos;GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root.1.com' WITH GRANT OPTION;"` 
7. **Popular** la base de datos con este otro comando `mysql -u root ospos < /var/www/html/database/database.sql` y 
despues el de CxC u Otro que siemrpe sera `"database_<algo>.sql"` en este caso CxC asi: `mysql -u root ospos < /var/www/html/database/database_tocredit.sql` 
8. **Configurar** las credenciales de acceso en el sistema ospos ejecutando este comando: 
para el usuario db asi: `sed -i "s/admin/root/g" /var/www/html/application/config/database.php` 
para la clave db asi: `sed -i "s/pointofsale/root.1.com/g" /var/www/html/application/config/database.php`
y despues ejecutar `mysqladmin -u root -h localhost password root.1.com`
9. **Crear** una entrada index.php para el directorio "public" asi: 
``` 
cat <<EOF > /var/www/html/index.php
<?php
\$queryString =  \$_SERVER['QUERY_STRING'];   
header("Location:public/index.php?".\$queryString);
die();
?>
EOF
```
10. **Afinar** la regla "rewrite" en el archivo ".htaccess" del directorio "public" en `/var/www/html/public/.htaccess` esto 
es verificar que la linea descomentada sea la que diga que este en root.
11. **Permisos** ajustados en el directorio apra el servicio web, con el comando 
asi: `chown -R www-data:www-data /var/www/html`
11. **Navegar** en el firefox ya abierto, dar click a el enclace que diga "index.php" en pantalla o 
a este enlace aqui: `http://localhost/public` or better `http://127.0.0.1/public` 
10. **Login** username as **admin**  and the password are **pointofsale** and then enjoy the software.

# Posibles errores (/var/log/apache2/error.log)

**MPORTANTE** si redescarga el codigo verificar en application/config/config.php el index path este de vacio a index.php


## error de sesion start

Si aparece el mensaje:

```
[:error] [pid 7053] [client 127.0.0.1:58422] PHP Fatal error:  session_start(): 
Failed to initialize storage module: user (path: sessions) 
in /var/www/html/vendor/codeigniter/framework/system/libraries/Session/Session.php on line 
```

Debe verificar la base de datos, esto:

1. el acceso del usuario sea correcto: root y clave tambien
2. que este usuario pueda usar la base de datos ospos
3. que la base de datos ospos este configurada y llena

Intentar repitiendo los pasos 6 al 9  y depsues corroborar esta secuencia:

1. ejecute `mysql -u root -p -D ospos` e ingrese la clave configurada en el paso 7 a 8 .
2. si logra entrar ejecute en el mysql `select username from ospos_employees;` y pulse enter
3. si en este punto ya logra ver "admin" todo esta corregido, sino revisar activacion de log en config.php

## Error redireccion 500 al iniciar sesion, pagian en blanco

si inicia sesion bien pero aparece la pagina en blanco puede deberse a varios motivos 
entre ellos el famoso error 500 pero aqui al tener todos los modulos el error es por otra razon:

https://github.com/opensourcepos/opensourcepos/issues/1927

sucede en ospos 3.2.0 antes de el 2018-04-05, y hay que aplicar los 2 commits que salen estos son los cambios:

2) SI EL OSPOS ES mas VIEJO QUE EL 03-04-2018 y la iso es menor del dia 05-04-2018 cambien en los archivos 
el `application/migrations/20180225100000_upgrade_to_3_2_0.php` y 
el `application/migrations/20180217100000_upgrade_to_3_2_0.php` asi:


``` diff

			$statement = $statement . ';';

-			if(!$this->db->simple_query($statement))
+			$CI = get_instance();
+			if(!$CI->db->simple_query($statement))
			{
-				foreach($this->db->error() as $error)
+				foreach($CI->db->error() as $error)
				{
					error_log('error: ' . $error);
				}
```

**DESPUES UNA DE LOS DOS SE RENOMBRA** prefeerible el segundo `application/migrations/20180217100000_upgrade_to_3_2_0.php` asi:
`mv application/migrations/20180217100000_upgrade_to_3_2_0.php application/migrations/20180217100000_upgrade_to_3_2_0_2.php` y 
se edita para cambiar el nombre de la clase asi:
``` diff
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

-class Migration_Upgrade_To_3_2_0 extends CI_Migration
+class Migration_Upgrade_To_3_2_0_2 extends CI_Migration
{

```


3) SI EL OSPOS ES MAS ALLA DEL 04-02-2018 pero mas VIEJO QUE EL 05-04-2018 cambien en `application/helpers/migration_helper.php` asi:

``` diff
@@ -2,6 +2,8 @@
 
 function execute_script($path)
 {
+	$CI = get_instance();
+
 	$version = preg_replace("/(.*_)?(.*).sql/", "$2", $path);
 	error_log("Migrating to $version");
 
@@ -19,14 +21,14 @@ function execute_script($path)
 	{
 		$statement = $statement . ';';
 
-		if(!$this->db->simple_query($statement))
+		if(!$CI->db->simple_query($statement))
 		{
-			foreach($this->db->error() as $error)
+			foreach($CI->db->error() as $error)
 			{
 				error_log('error: ' . $error);
 		
	}

```