# Debian 8 and Androit Studio 2.X installation

LA version de androitstudio 2.X especiamente la 2.3.3 la ultima estable de la rama 2 es 
la recomendada para trabajar en debian jessie 8, o devuan 1 jessie, asi como debian wheeze, 

Cabe destacar que este no soporta Androit 5.X sino apenas el 5.0 sin embargo no es mucho 
lo que se pierde ademas de que los cambios son leves, a menos que maneje el hardware del propio telefono.

El IDE es 32bit no 64bit, por lo que la instalacion difiere muchisimo si usa debian wheeze 
meintras que si usa debian jessie o devuan solo simpelmente hay que agregar otra arquitectura.

## Debian 7, 8 y 9 Java entorno

Java consta de dos partes siempre, igual que un software que consta de la libreria de uso 
y su compañera para desarrollo, esta el JDK que incluia el JRE.

* **JRE** Entorno de ejecucion, este solo es para ejecutar o correr aplicaciones ya fabricadas
* **JDK** Entorno de desarrollo, este permite fabricar las aplicaciones java

Antes solo era eso y listo, el JDK obviamente incluye el JRE, hoy dia hay varios "sabores" de JDK 
como lo son para mobiles pero que hoy dia evoluciono a Androit Studio por ejemplo.
Tambien hay varios "colores" como lo son el sin fin de librerias y adiciones a el JDK 
pro ejemplos "java-commons" o sino utilidades como "junit" y un fastidioso etc..

**NOTA** Hoy esta el serv er JRE, es simplemente uan version minimalista y comun del JRE normal, 
algo asi como "sin instalador y sin dependencia grafica) esto dado que la filosofia unix de que 
el servidor no es para correr cosas graficas si es un servidor web.. 
ya que el servicio de graficos esta en su propio servidor (hoy dia esto es Xorg y se trabaja casi 
como si fuera una app en vez de un servicio) Asi que 
si ya instalan el JRE no necesitan Server-JRE: https://blogs.oracle.com/java-platform-group/understanding-the-server-jre

* **Debian 7**: este no trae JAva 7 ni 8, menos 9, el OpenJDK que trae es el 6, para instalar 
el 7 habria que tener activados el backports, mas abajo se explica esto.
* **Debian 8**: viene con JAva 7, pero no trae JAva 8 ni 9, para instalar el 8 hay que tener 
activados el backports, mas abajo se exlica esto.
* **Debian 9**: este viene con Java 9 el OpenJDK.

Para tener por el contrario el sabor de Oracle/Sun tenemos que usar el **java-package**.

# 1.- Usar el Java Entorno OracleJ2SE

Aunque es el OpenJDK y es el que comanda y determina el otro OracleJDK 
seguramente alguna trampa estara y por eso podemos realizarlo con Oracle tambien.

Para este se usara el java-package aunque la guia esta cambia en el tiempo y esta 
es especifica, se deja el link para referencia de esta: https://wiki.debian.org/JavaPackage

## preparar dependencias y entorno preparado

`apt-get install libcanberra-gtk3-module libcanberra-gtk-module libcanberra-gtk3-dev libcanberra-gtk-dev`

`apt-get install libgl1-mesa-glx libfontconfig1 libxslt1.1 libxtst6 libxxf86vm1 libgtk2.0-0`

`apt-get install java-package java-common ca-certificates-java ant ant-optional bsh junit4`

## Instalar OracleJ2SE

Este comando no debe ser ejecutado como root:

`make-jpkg --revision 0 jdk-8u162-linux-i586.tar.gz`

Este comando si es como root y los siguientes o anteriores

`dpkg -i oracle-java8-jdk-8u162-1*.deb`

## Testeando OracleJ2SE

`update-java-alternatives -s oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH)`

`java -version`

Mostrara en la salida:
```
java version "1.8.0_162"
Java(TM) SE Runtime Environment (build 1.8.0_162-b12)
Java HotSpot(TM) Server VM (build 25.162-b12, mixed mode)
```

## configurar entorno OracleJava

`echo "JAVA_HOME=/usr/lib/jvm/oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH)" >> /etc/environment`


# 2.- instalacion androit studio

`apt-get install unzip wget mesa-utils`

`apt-get purge gobjc++-multilib gobjc++-4.9-multilib g++-multilib g++-4.9-multilib gobjc-multilib gobjc-4.9-multilib gcc-multilib gcc-4.9-multilib`

Para 32bit:

`apt-get install libncurses5-dev libncursesw5-dev libbz2-dev bzip2 libzip-dev zlib1g-dev libstdc++4.8-dev libstdc++4.9-dev`

Para 64bit:

`apt-get install libncurses5-dev:i386 libncursesw5-dev:i386 libbz2-dev:i386 libstdc++4.8-dev:i386 libstdc++4.9-dev:i386 libc6:i386 libstdc++6:i386 lib32z1`

Descargar y ejecutar el studio con el mogollon y mudar a opt alli descomprimir asi (root):

`mkdir -p /opt/google/androitsdk`

`cd /opt/google`

`wget https://dl.google.com/dl/android/studio/ide-zips/2.3.3.0/android-studio-ide-162.4069837-linux.zip`

`unzip android-studio-ide-162.4069837-linux.zip`

`export PATH="$PATH:/opt/google/android-studio/bin"`

`chown root:users /opt/google/android-studio/bin`

`/opt/google/android-studio/bin/studio.sh`

Despues de consumir considerablemente tiempo e innecesariamente espacio mostrara 
un asistente, estas son las preguntas estandar:

* importar configuracion: contestar que `do not have a previous...`
* tipo de instalacion: escoger la `Custom` **ADVERTENCIA DEBE SELECCIONAR CUSTOM**
* si ofrece donde instalar los componentes: en `/opt/google/androitsdk`
* **Deseleccionar el Emulador Androit**, es ESTUPIDEZ IDE EN 32BIT Y EMULADOR EN 64BIT
* despues muestra un resumen y la advertencia de que puede usar KVM, dele finish

En la pantalla de instalacion dar a detalles para ver si ocurren errores, 
eso tardara, porque la estupidez es tan grande que a pesar de ser un instalador 
con 700 megas aun baja mas desde internet!


# 3.- Primera ejecucion, emulador y ajustes

Antes de ejecutar el IDE hay que preconfigurar algunas dependencias.

Ejecutamos con

`ANDROID_EMULATOR_FORCE_32BIT=true JAVA_HOME=/usr/lib/jvm/oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH) /opt/android-studio/bin/studio.sh`

## usar el ultimo gradle

`dpkg --purge  gradle`

`update-java-alternatives -s oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH)`

Descargar el gradle desde internet el binario, meterlo en /opt/google y en el ide 
ir a Menu->Proyect structure->JDK colocarle la ruta del JDK oracle que esta
en `/usr/lib/jvm/oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH)`

borrar gragle de home y tambien del proyecto, despues arrancar el ide 

## instalar el androit emulador del studio

`apt-get install libncurses5-dev:i386 libncursesw5-dev:i386 libbz2-dev:i386 bzip2`

`apt-get install libxcb1-dev:i386 zlib1g-dev:i386 libstdc++4.8-dev:i386 libstdc++4.9-dev:i386`

`apt-get purge gobjc++-multilib gobjc++-4.9-multilib g++-multilib g++-4.9-multilib gobjc-multilib gobjc-4.9-multilib gcc-multilib gcc-4.9-multilib`

En sistemas 32bit necesita esto:

`echo "export ANDROID_EMULATOR_FORCE_32BIT=true" >> ~/.profile`

Despues lanzar el ide con `ANDROID_EMULATOR_FORCE_32BIT=true JAVA_HOME=/usr/lib/jvm/oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH) /opt/android-studio/bin/studio.sh`

Muestra una **pantalla de inicio, alli dar click al boton de configuracion**, 
ir a `Appearance & behaviour` -> `System settings` -> `Androit SDK` -> `SDK tools`
alli seleccionar `Androit emulador` si ya esta seleccionado deseleccionar y volver seleccionar.
entonces pulsar `Apply` y esperar.

**ADVERTENCIA: no soporta USB ni Bluetooh** es decir no sirve el emulador para esto.

Se debe probar directo en el dispositivo.

## Ejecutar proyecto CUIDADO ajuste de API/SDK

Cada proyecto tiene un nivel de api, pero esto requiere un SDK especifico, 
asi que hay que descargarlos para cada version androit OS, esto es en menu superior 
y alli ir a `Appearance & behaviour` -> `System settings` -> `Androit SDK` y 
en la ventana escoger `Androit X.0` donde X es la version del OS en el telefono 
seleccionar el recuadro y pulsar `Apply`.

## instalar plantilla basica Kotlin

Despues de lanzar el ide y escoger un proyecto, ir a el menu superior y alli 
ir a `Tools` -> `Kotlin` -> `Configure Kotlin updates` y en la ventana 
escoger `Check for updates` despues de descargar aparecera un boton `Install` 
el cual hay que pulsar para tener dichas plantillas.

# 4.- Conclusiones: malo muy malo

Es el peor ide de la historia, el consumo es su marca, la dependencias es su chillantina, 
descarga un monton de cosas pero funciona es online, y para peor e ilogicamente 
el ide es 32bit pero el emulador es 64bit, mezcla un monton de dependencias.

Hay otros sistemas muchomejores para desarrollar en androit, claro esta menos 
estupidos y automatizados, no para bebes como el androitstudio..