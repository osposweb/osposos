# Java y Debian

Java consta de dos partes siempre, igual que un software que consta de la libreria de uso 
y su compañera para desarrollo, esta el JDK que incluia el JRE.

* **JRE** Entorno de ejecucion, este solo es para ejecutar o correr aplicaciones ya fabricadas
* **JDK** Entorno de desarrollo, este permite fabricar las aplicaciones java

Antes solo era eso y listo, el JDK obviamente incluye el JRE, hoy dia hay varios "sabores" de JDK 
como lo son para mobiles pero que hoy dia evoluciono a Androit Studio por ejemplo.
Tambien hay varios "colores" como lo son el sin fin de librerias y adiciones a el JDK 
pro ejemplos "java-commons" o sino utilidades como "junit" y un fastidioso etc..

**NOTA** Hoy esta el serv er JRE, es simplemente uan version minimalista y comun del JRE normal, 
algo asi como "sin instalador y sin dependencia grafica) esto dado que la filosofia unix de que 
el servidor no es para correr cosas graficas si es un servidor web.. 
ya que el servicio de graficos esta en su propio servidor (hoy dia esto es Xorg y se trabaja casi 
como si fuera una app en vez de un servicio) Asi que 
si ya instalan el JRE no necesitan Server-JRE: https://blogs.oracle.com/java-platform-group/understanding-the-server-jre

* **Debian 7**: este no trae JAva 7 ni 8, menos 9, el OpenJDK que trae es el 6, para instalar 
el 7 habria que tener activados el backports, mas abajo se explica esto.
* **Debian 8**: viene con JAva 7, pero no trae JAva 8 ni 9, para instalar el 8 hay que tener 
activados el backports, mas abajo se exlica esto.
* **Debian 9**: este viene con Java 9 el OpenJDK.

Para tener por el contrario el sabor de Oracle/Sun tenemos que usar el **java-package**.


# 1.- Usar el JOracleJSE

Este paquete es solo binario, y aunque tambien se puede compilar desde las fuentes su licencia 
es lo que no permite redistribuirlo normalmente, por lo que fue removido de debian.

OpenJDK es segun el mismo oracle el matrix de el oracle JSE pero ilogicamnete sus productos 
no son siempre compatible con este OpenJDK ilogicamente.. clar porque es software libre 
y no tiene sentido promover un producto que no es el suyo propio..

**La ventaja es que con este se puede realizar la instalacion de igual forma sin dolor 
en cualqueir debian, incluso en wheeze o squeeze asi como jessie y strecht**

Aunque es el OpenJDK y es el que comanda y determina el otro OracleJDK 
seguramente alguna trampa estara y por eso podemos realizarlo con Oracle tambien.

Para este se usara el java-package aunque la guia esta cambia en el tiempo y esta 
es especifica, se deja el link para referencia de esta: https://wiki.debian.org/JavaPackage

## preparar dependencias y entorno preparado

`apt-get install libcanberra-gtk3-module libcanberra-gtk-module libcanberra-gtk3-dev libcanberra-gtk-dev`

`apt-get install libgl1-mesa-glx libfontconfig1 libxslt1.1 libxtst6 libxxf86vm1 libgtk2.0-0`

`apt-get install java-package java-common ca-certificates-java ant ant-optional bsh junit4`

**NOTA:** en wheeze o squeeze es recomendable descargarlo directamente ya que la version puede no sorporte.

Descarga: http://ftp.us.debian.org/debian/pool/contrib/j/java-package/java-package_0.56_all.deb

## Instalar OracleJ2SE

Este comando no debe ser ejecutado como root:

`make-jpkg --revision 0 jdk-8u162-linux-i586.tar.gz`

Este comando si es como root y los siguientes o anteriores

`dpkg -i oracle-java8-jdk-8u162-1*.deb`

## Testeando OracleJ2SE

`update-java-alternatives -s oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH)`

`java -version`

Mostrara en la salida:
```
java version "1.8.0_162"
Java(TM) SE Runtime Environment (build 1.8.0_162-b12)
Java HotSpot(TM) Server VM (build 25.162-b12, mixed mode)
```

## configurar entorno OracleJava

`echo "JAVA_HOME=/usr/lib/jvm/oracle-java8-jdk-$(dpkg-architecture -q DEB_HOST_ARCH)" >> /etc/environment`



# 2.- Usar el OpenJDK en debian

OpenJDK es segun el mismo oracle el matrix de el oracle JSE pero ilogicamnete sus productos 
no son siempre compatible con este OpenJDK ilogicamente.. clar porque es software libre 
y no tiene sentido promover un producto que no es el suyo propio..

Aunque no lo crean primero es el OpenJDK y es el que comanda y determina el otro OracleJDK 
lo que es ilogico que Oracle haga cosas incompatibles con el OpenJDK.

Cuando la version de OpenJDK se actualiza, la de Oracle tabien, es decir no hay 
actualizaciones que no pasen por OpenJDK, es decir OpenJDK esta mas al dia.

## preparar dependencias y entorno preparado

`apt-get install libcanberra-gtk3-module libcanberra-gtk-module libcanberra-gtk3-dev libcanberra-gtk-dev`

`apt-get install libgl1-mesa-glx libfontconfig1 libxslt1.1 libxtst6 libxxf86vm1 libgtk2.0-0`

## Activar backports

Para Jessie, para el resto cambiar "jessie" por "wheeze" o "stretch" o incluso "squeeze":

```
echo "deb http://ftp.de.debian.org/debian jessie-backports main contrib non-free" > /etc/apt/sources.list.d/backports.list

cat > /etc/apt/preferences.d/backports << EOF
Package: *
Pin: release a=jessie-backports,n=jessie-backports
Pin-Priority: 500
EOF
```

## Instalar OpenJDK


En jessie o debian/devuan 8 el JDK/JRE es 7 (1.7.0), por lo que el backport que ofrecen es 8 (1.8.0), mientras 
que en wheeze o debian 7 el JDK/JRE es 6 (1.6.0), po lo que el backport que ofrecen es 7 (1.7.0), y asi..

**Para jeesie**:

`apt-get install openjdk-8-jre openjdk-8-jre-headless ca-certificates-java`

`apt-get install openjdk-8-jdk openjdk-8-jdk-headless openjdk-8-source`

`apt-get install java-common openjdk-8-demo ant ant-optional openjfx junit bsh javacc junit4`

**Para wheeze**:

`apt-get install openjdk-7-jre openjdk-7-jre-headless ca-certificates-java`

`apt-get install openjdk-7-jdk openjdk-7-jdk-headless openjdk-7-source`

`apt-get install java-common openjdk-7-demo ant ant-optional openjfx junit bsh javacc junit4`

Despues configurar el entorno

## Testeando OpenJDK

**Para jessie**:

`update-java-alternatives -s java-1.8.0-openjdk-$(dpkg-architecture -q DEB_HOST_ARCH)`

`java -version`

Mostrara en la salida
```
openjdk version "1.8.0_131"
OpenJDK Runtime Environment (build 1.8.0_131-8u131-b11-1~bpo8+1-b11)
OpenJDK Server VM (build 25.131-b11, mixed mode)
```

**Para wheeze**:

`update-java-alternatives -s java-1.7.0-openjdk-$(dpkg-architecture -q DEB_HOST_ARCH)`

`java -version`

Mostrara en la salida
```
openjdk version "1.7.0_111"
OpenJDK Runtime Environment (build 1.7.0_111-7u111-b11-1~bpo7+1-b11)
OpenJDK Server VM (build 22.111-b11, mixed mode)
```

## configurar entorno OpenJdk

**Para jeesie**:

`echo "JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-$(dpkg-architecture -q DEB_HOST_ARCH)" >> /etc/environment`

**Para wheeze**:

`echo "JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-$(dpkg-architecture -q DEB_HOST_ARCH)" >> /etc/environment`

