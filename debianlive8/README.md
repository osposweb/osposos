##  Debian VENENUX MATE Desktop with OSPOSWEB

Este directorio construye un debian una imagen ISO/USB mix, como si de un disco duro se tratase, 
se revise el directorio `debianlive` por ejemplo para jessie [debianlive8](debianlive8) 
esta instalacion trae `mate`,  `apache2`, `php`, `mariadb`, `firefox`, `git`, y `osposweb` y 
este hay que instalarlo antes de usarlo, tambien trae software de redes y multimedia.

## Requerimientos

A software called live-build can be used to automatically build images from
this configuration tree.

  # apt-get install live-build

live-build can be used to build this image with the following command executed
in this directory::

  # lb build

20Gb de espacio
internet minimo 100Kbps
1GHz CPU minimo
1Gb swap minimo

## Notas del proceso

El proceso usado esta en el archivo [../README-2-lb-build.md](../README-2-lb-build.md)
Para mayor informacion visitar el proyecto @venenux !

Este depende totalmente de internet, para uno sin internet visitar el trabajo en progreso 
en el directorio [../debian6diskoff](../debian6diskoff) que teniendo solo las isos y fuentes 
intenta hacer todo sin internet.
