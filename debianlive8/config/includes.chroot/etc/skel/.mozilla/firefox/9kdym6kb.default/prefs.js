# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 */

user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 0);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 0);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.capacity", 1048576);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.frecency_experiment", 1);
user_pref("browser.download.importedFromSqlite", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1519415480);
user_pref("browser.laterrun.bookkeeping.sessionCount", 2);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 42);
user_pref("browser.newtabpage.enhanced", false);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.rights.3.shown", true);
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20180123215146");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1519415517");
user_pref("browser.slowStartup.averageTime", 6048);
user_pref("browser.slowStartup.samples", 2);
user_pref("browser.startup.homepage_override.buildID", "20180123215146");
user_pref("browser.startup.homepage_override.mstone", "52.6.0");
user_pref("browser.tabs.remote.autostart.2", true);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"PanelUI-contents\":[\"edit-controls\",\"zoom-controls\",\"new-window-button\",\"privatebrowsing-button\",\"save-page-button\",\"print-button\",\"history-panelmenu\",\"fullscreen-button\",\"find-button\",\"preferences-button\",\"add-ons-button\",\"developer-button\",\"sync-button\"],\"addon-bar\":[\"addonbar-closebutton\",\"status-bar\"],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"urlbar-container\",\"search-container\",\"bookmarks-menu-button\",\"downloads-button\",\"home-button\",\"pocket-button\",\"abp-toolbarbutton\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"toolbar-menubar\":[\"menubar-items\"]},\"seen\":[\"pocket-button\",\"abp-toolbarbutton\",\"developer-button\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\",\"PanelUI-contents\",\"addon-bar\"],\"currentVersion\":6,\"newElementCount\":0}");
user_pref("browser.urlbar.daysBeforeHidingSuggestionsPrompt", 3);
user_pref("browser.urlbar.lastSuggestionsPromptDate", 20180223);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1519415527264");
user_pref("datareporting.sessions.current.activeTicks", 3);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.sessions.current.firstPaint", 3282);
user_pref("datareporting.sessions.current.main", 190);
user_pref("datareporting.sessions.current.sessionRestored", 3114);
user_pref("datareporting.sessions.current.startTime", "1519415514134");
user_pref("datareporting.sessions.current.totalTime", 13);
user_pref("datareporting.sessions.currentIndex", 1);
user_pref("datareporting.sessions.previous.0", "{\"s\":1519415474647,\"a\":5,\"t\":32,\"c\":true,\"m\":962,\"fp\":9179,\"sr\":9099}");
user_pref("e10s.rollout.cohort", "addons-set51set1-test");
user_pref("e10s.rollout.cohortSample", "0.257653");
user_pref("experiments.activeExperiment", false);
user_pref("extensions.adblockplus.currentVersion", "2.7.3");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.bootstrappedAddons", "{\"aushelper@mozilla.org\":{\"version\":\"2.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox-esr/browser/features/aushelper@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"e10srollout@mozilla.org\":{\"version\":\"1.10\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox-esr/browser/features/e10srollout@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"firefox@getpocket.com\":{\"version\":\"1.0.5\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox-esr/browser/features/firefox@getpocket.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"webcompat@mozilla.org\":{\"version\":\"1.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox-esr/browser/features/webcompat@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-de@firefox-esr.mozilla.org\":{\"version\":\"52.6.0\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox-esr/browser/extensions/langpack-de@firefox-esr.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-es-AR@firefox-esr.mozilla.org\":{\"version\":\"52.6.0\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-AR@firefox-esr.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-es-CL@firefox-esr.mozilla.org\":{\"version\":\"52.6.0\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-CL@firefox-esr.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-es-ES@firefox-esr.mozilla.org\":{\"version\":\"52.6.0\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-ES@firefox-esr.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-es-MX@firefox-esr.mozilla.org\":{\"version\":\"52.6.0\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-MX@firefox-esr.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-ru@firefox-esr.mozilla.org\":{\"version\":\"52.6.0\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox-esr/browser/extensions/langpack-ru@firefox-esr.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}\":{\"version\":\"2.7.3\",\"type\":\"extension\",\"descriptor\":\"/usr/share/xul-ext/adblock-plus\",\"multiprocessCompatible\":true,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false}}");
user_pref("extensions.databaseSchema", 19);
user_pref("extensions.e10s.rollout.blocklist", "{dc572301-7619-498c-a57d-39143191b318};support@lastpass.com;");
user_pref("extensions.e10s.rollout.hasAddon", true);
user_pref("extensions.e10s.rollout.policy", "51set1");
user_pref("extensions.e10sBlockedByAddons", false);
user_pref("extensions.enabledAddons", "%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:52.6.0");
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppVersion", "52.6.0");
user_pref("extensions.lastPlatformVersion", "52.6.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.xpiState", "{\"app-system-defaults\":{\"aushelper@mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/features/aushelper@mozilla.org.xpi\",\"e\":true,\"v\":\"2.0\",\"st\":1516785519000},\"e10srollout@mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/features/e10srollout@mozilla.org.xpi\",\"e\":true,\"v\":\"1.10\",\"st\":1516785519000},\"firefox@getpocket.com\":{\"d\":\"/usr/lib/firefox-esr/browser/features/firefox@getpocket.com.xpi\",\"e\":true,\"v\":\"1.0.5\",\"st\":1516785519000},\"webcompat@mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/features/webcompat@mozilla.org.xpi\",\"e\":true,\"v\":\"1.0\",\"st\":1516785519000}},\"app-global\":{\"langpack-de@firefox-esr.mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/langpack-de@firefox-esr.mozilla.org.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516761881000},\"langpack-es-AR@firefox-esr.mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-AR@firefox-esr.mozilla.org.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516761916000},\"langpack-es-CL@firefox-esr.mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-CL@firefox-esr.mozilla.org.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516761922000},\"langpack-es-ES@firefox-esr.mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-ES@firefox-esr.mozilla.org.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516761927000},\"langpack-es-MX@firefox-esr.mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/langpack-es-MX@firefox-esr.mozilla.org.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516761933000},\"langpack-ru@firefox-esr.mozilla.org\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/langpack-ru@firefox-esr.mozilla.org.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516762205000},\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"d\":\"/usr/lib/firefox-esr/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}.xpi\",\"e\":true,\"v\":\"52.6.0\",\"st\":1516785519000}},\"app-system-share\":{\"{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}\":{\"d\":\"/usr/share/xul-ext/adblock-plus\",\"e\":true,\"v\":\"2.7.3\",\"st\":1519422154000,\"mt\":1471316140000}}}");
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.cookie.prefsMigrated", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.history.expiration.transient_current_max_pages", 17052);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("signon.importedFromSqlite", true);
user_pref("toolkit.startup.last_success", 1519415514);
user_pref("toolkit.telemetry.previousBuildID", "20180123215146");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
