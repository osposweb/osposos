### 1 Requisitos para instalar

Si instala todo lo aqui espuesto tendra ya todo para instalacion de el osposweb:

* mysql (manejador y servidor DB que hara de pivote) `apt-get install mysql-client mysql-server mysql-workbench`
* php7/php5, gd, mcrypt, mysql, curl, xml, intl (php y modulos de uso) `apt-get isntall php5-mysql php5-mcrypt php5-curl php5-gd php5-intl`
* apache2 (servidor web usar lastimosamente) `apt-get install libapache2-mod-php5 apache2-bin apache2`

Decargar el ospos desde el oficial o desde el clone de gitlab osposweb:

Setup/Add the admin user name and password for MySQL and phpMyAdmin.

### 2 Configurar tu entorno

Habilitar los servicios de mysql y apache:

`systemctl enable mysql;systemctl enable apache2`

Crear el usuario y accesos para el ospos

``` sql
    CREATE USER 'ospos'@'%'
        IDENTIFIED BY PASSWORD  'ospos.1';
    GRANT 
        USAGE ON * . * TO  'ospos'@'%' 
        IDENTIFIED BY PASSWORD  'ospos.1' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;
    GRANT 
        ALL PRIVILEGES ON  * . * TO  'ospos'@'%' 
        WITH GRANT OPTION ;
```

Configure Date and Time according to your location for the server to run correctly.

`dpkg-reconfigure tzdata`

 Select your time Zone Modify the PHP files for the Time zone.

`nano /etc/php5/cli/php.ini`

 and find "Date" by scrolling down or by Ctrl+W and remove `;` from the line 
and modify like this `date.timezone = America/Caracas` by example.

do the same with `/etc/php5/apache2/php.ini`

restart Apache

`service apache2 restart`.

Visitar el `http://127.0.0.1/`para verificar todo este bien.

Configurar el usuario de datos de html root:

`chown -R www-data /var/www/html`

### 3 obtener e instalar las fuentes

Descargar el ospos y depues debe descomprimirse todo el contenido 
en la raiz del htdocs es decir en `/var/www/html` directamente

`chown -R www-data /var/www/html`

Conectarse y crear la db

`mysql -u ospos -p`

``` sql
CREATE SCHEMA ospos;
```

Cargar el script de la base de datos en la recien creada

`mysql -u ospos -pospos.1 ospos < /var/www/html/database/database.sql`

**NOTA** caulquier otro script adicional debe cargarse ocmo el de fiscal!

Modify `application/config/database.php` to connect to your database.
nano opensourcepos/application/config/database.php.
go to the bottom of the file and modify this to your database

Alli colocar usuario calve y nombre dejar el prefijo en ospos_

Y despues visitar con el navegador `http://127.0.0.1/`para verificar todo este bien.
